<?php include 'templates/top.inc.php'; ?>


<div class="osin">
    <div class="container-fluid main-photo">
        <div class="col-xs-12">


            <div class="row">


                <div class="container-fluid photo-profile">
                    <div class="col-md-3 col-xs-12">
                        <img class="img-circle img-vk"
                             src="images/profile.jpg">

                    </div>
                </div>

                <div class="col-xs-12 caption">
                    <h1><b>Yuliia Zelenska</b></h1>

                    <div class="info"></br>
                        <button class="btn" id="btn-1">INFO</button>
                        <button class="btn" id="btn-2">CONTACT</button>
                        <button class="btn" id="btn-3">EDUCATION</button>
                        <button class="btn" id="btn-4">SKILLS</button>
                        <button class="btn" id="btn-5">EXPERIENCE</button>
                    </div>


                    <div class="conntent-info hidden">
                        <h3><a target="_blank" href="https://bitbucket.org/JuliaKodi/cv/commits/all">Projekt tej strony
                                na
                                BitBucket<i class="fa fa-bitbucket"></i> </a></h3>
                    </div>
                    <div class="conntent-contact hidden">
                        <ul>
                            <li><a href="tel:+48790390343">+48 790 390 343</a></li>
                            <li><a href="mailto:julietbondarenko@gmail.com">julietbondarenko@gmail.com</a></li>

                            <li>
                                <a target='_blank' href="https://www.facebook.com/profile.php?id=100008317809451">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                                <a target='_blank' href="https://github.com/JuliaKodi">
                                    <i class="fa fa-github" aria-hidden="true"></i>
                                </a>
                                <a target='_blank' href="https://bitbucket.org/account/user/JuliaKodi/">
                                    <i class="fa fa-bitbucket" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>

                    </div>

                    <div class="conntent-education hidden">
                        <a href="http://wsb.edu.pl" target='_blank'><img style="width: 80px; height: 80px"
                                                                         clsss="img-wsb"
                                                                         src="import/WSB.png"></a>
                        <ul>
                            <h4><B>Wyższa Szkoła Biznesu w Dąbrowie Górniczej</B></h4>

                            <li>2014 > Sieci komputerowe i bazy danych > 2018</li>

                            <li>2014 > International business > 2016</li>
                        </ul>


                    </div>

                    <div class="conntent-skills hidden">
                        <ul>

                            <li>ukrainski, rosyjski - C2</li>
                            <li>polski - C1</li>
                            <li>angielski - B1</li>

                            <li>PHP, HTML, CSS, LESS, Bootstrap, JavaScript, jQuery, SQL, Git</li>
                        </ul>
                    </div>


                </div>

                <div class="conntent-experience hidden">
                    <ul>
                        <li><a href="http://xdev.me/projekty/Piekarnia/">Piekarnia Projekt</a></li>
                        <li><a href="http://xdev.me/projekty/moj_produkt/">Mój produkt</a></li>
                </div>
            </div>
        </div>

        <?php include 'templates/bottom.inc.php'; ?>
